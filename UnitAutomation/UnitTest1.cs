﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;  

namespace UnitAutomation
{
    [TestFixture]
    
    public class UnitTest1
    {
        IWebDriver driver;

        [SetUp]
        public void SetUp() {

            driver = new ChromeDriver();
        }

        [Test]
        public void AutomationTest()
        {
            driver.Url = "http://automationpractice.com/index.php";
            IWebElement contact = driver.FindElement(By.Id("contact-link"));

            contact.Click();

            if (driver.Title.Equals("Contact us - My Store"))
            {
                IWebElement selectBox = driver.FindElement(By.Id("id_contact"));
                IWebElement txtEmail = driver.FindElement(By.Id("email"));
                IWebElement txtIdOrder = driver.FindElement(By.Id("id_order"));
                IWebElement txtMessage = driver.FindElement(By.Id("message"));
                IWebElement btnSend = driver.FindElement(By.XPath("//span[contains(text(),'Send')]"));

                new SelectElement(selectBox).SelectByText("Webmaster");
                txtEmail.SendKeys("wmisari@belatrixsf.com");
                txtIdOrder.SendKeys("1234");
                txtMessage.SendKeys("Test");
                btnSend.Click();

                IWebElement alert = driver.FindElement(By.XPath("//div[@id='center_column']/p"));
                String texto = alert.Text;
                Assert.AreEqual(texto, "Your message has been successfully sent to our team.");


            }
        }

        [TearDown]
        public void Clear()
        {
           driver.Quit();
        }

    }
}
